//
//  LoginModel.swift
//  Timenizer
//
//  Created by Sergio Escalante Ordonez on 23/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation

enum PasswordError {
    case MinimumCharacters
    case NotEqual
}

struct LoginModel {
    var name: String?
    var email: String
    var password: String
    var confirmPassword: String?
    var remember: Bool
    var newUser: Bool
    
    func validPassword() -> (Bool, PasswordError?) {
        return (true, nil)
    }
    
    func validEmail() -> Bool {
        return true
    }
}
