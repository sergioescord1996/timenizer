//
//  AddReminderViewController.swift
//  Timenizer
//
//  Created by Sergio Escalante Ordonez on 24/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

class AddReminderViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var descriptionTextView: CustomTextView!
    @IBOutlet weak var dateTextField: CustomTextField!
    @IBOutlet weak var typeTextField: CustomTextField!
    @IBOutlet weak var prioritySwitch: UISwitch!
    @IBOutlet weak var periodicallySwitch: UISwitch!
    @IBOutlet weak var addButton: UIButton!
    
    // MARK: - Variables
    var model = AddReminderModel()
    
    // MARK: - Constants
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    // MARK: - IBActions
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        // TODO: Check and save new reminder
    }
    
    @IBAction func prioritySwitchPressed(_ sender: UISwitch) {
        updateModel()
    }
    
    @IBAction func periodicallySwitchPressed(_ sender: UISwitch) {
        updateModel()
    }
    
    @IBAction func textFieldDidChanged(_ sender: UITextField) {
        updateModel()
        validateForm()
    }
    
    // MARK: - Custom methods
    
    func configureUI() {
        nameTextField.delegate = self
        descriptionTextView.delegate = self
        dateTextField.delegate = self
        typeTextField.delegate = self
        
        dateTextField.datePicker(target: self,
                                 doneAction: #selector(doneActionDate),
                                 cancelAction: #selector(cancelActionDate),
                                 datePickerMode: .dateAndTime)
        
        typeTextField.pickerView(target: self,
                                 doneAction: #selector(doneActionType),
                                 cancelAction: #selector(cancelActionType))
    }
    
    func validateForm() {
        addButton.isEnabled = model.validate()
    }
    
    func updateModel() {
        model.update(name: nameTextField.text,
                     date: dateTextField.text,
                     summary: descriptionTextView.text,
                     priority: prioritySwitch.isOn,
                     type: typeTextField.text,
                     periodically: periodicallySwitch.isOn)
    }
    
    // MARK: - DateTextField Methods
    
    @objc func cancelActionDate() {
        self.dateTextField.resignFirstResponder()
    }
    
    @objc func doneActionDate() {
        if let datePickerView = self.dateTextField.inputView as? UIDatePicker {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd, HH:mm"
            let dateString = dateFormatter.string(from: datePickerView.date)
            self.dateTextField.text = dateString
            
            self.dateTextField.resignFirstResponder()
        }
    }
    
    @objc func cancelActionType() {
        self.typeTextField.resignFirstResponder()
    }
    
    @objc func doneActionType() {
        if let _ = self.typeTextField.inputView as? UIPickerView {
            self.typeTextField.resignFirstResponder()
        }
    }
}

// MARK: - Extension UITextFieldDelegate

extension AddReminderViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateModel()
        validateForm()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
            case nameTextField:
                if let name = nameTextField.text, !name.isEmpty {
                    nameTextField.resignFirstResponder()
                    return false
                }
            default:
                textField.resignFirstResponder()
        }

        return false
    }
}

// MARK: - Extension UITextViewDelegate

extension AddReminderViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        updateModel()
    }
}

// MARK: - Extension UIPickerViewDatasourvce

extension AddReminderViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return model.getNumberOfTypes()
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return model.getType(at: row)
    }
}

// MARK: - Extension UIPickerDelegate

extension AddReminderViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // Set the option to textfield
        typeTextField.text = model.getType(at: row)
    }
}
