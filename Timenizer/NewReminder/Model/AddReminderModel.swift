//
//  AddReminderModel.swift
//  Timenizer
//
//  Created by Sergio Escalante Ordonez on 24/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation

struct AddReminderModel {
    var name: String
    var date: String
    var summary: String
    var priority: Bool
    var type: String
    var periodically: Bool
    
    init() {
        name = ""
        date = ""
        summary = ""
        type = ""
        priority = false
        periodically = false
    }
    
    func getNumberOfTypes() -> Int {
        return ReminderType.allCases.count
    }
    
    func getType(at position: Int) -> String {
        return ReminderType.allCases[position].rawValue.capitalized
    }
    
    func validate() -> Bool {
        return !name.isEmpty && !date.isEmpty && !type.isEmpty
    }
    
    mutating func update(name: String?, date: String?, summary: String?, priority: Bool?, type: String?, periodically: Bool?) {
        self.name = name ?? ""
        self.date = date ?? ""
        self.summary = summary ?? ""
        self.priority = priority ?? false
        self.type = type ?? ""
        self.periodically = periodically ?? false
    }
}
