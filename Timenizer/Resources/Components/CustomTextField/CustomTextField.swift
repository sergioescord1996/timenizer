//
//  CustomTextField.swift
//  Timenizer
//
//  Created by Sergio Escalante Ordonez on 24/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

@IBDesignable open class CustomTextField: UITextField {
    
    @IBInspectable var placeholderColor: UIColor = .lightGray {
        didSet {
            let placeholderStr = placeholder ?? ""
            attributedPlaceholder = NSAttributedString(string: placeholderStr, attributes: [NSAttributedString.Key.foregroundColor: placeholderColor])
        }
    }
    
    @IBInspectable var rightIcon: UIImage? {
        didSet {
            addRightImage()
        }
    }
    
    @IBInspectable var underline: Bool = false {
        didSet {
            addUnderline()
        }
    }
}

extension CustomTextField {
    
    func addRightImage() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.size.height, height: self.frame.size.height))
        imageView.image = rightIcon?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = self.textColor
        self.rightViewMode = .unlessEditing
        self.rightView = imageView
    }
    
    func addUnderline() {
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width-10, height: self.frame.size.height)
        
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    private func buttonItem(withSystemItemStyle style: UIBarButtonItem.SystemItem, action: Selector? = nil) -> UIBarButtonItem {
        let buttonTarget = style == .flexibleSpace ? nil : target
        
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: style,
                                            target: buttonTarget,
                                            action: action)
        
        return barButtonItem
    }
    
    private func addToolbar(cancelAction: Selector, doneAction: Selector, screenWidth: CGFloat) {
        let toolBar = UIToolbar(frame: CGRect(x: 0,
                                              y: 0,
                                              width: screenWidth,
                                              height: 44))
        toolBar.setItems([buttonItem(withSystemItemStyle: .cancel, action: cancelAction),
                          buttonItem(withSystemItemStyle: .flexibleSpace),
                          buttonItem(withSystemItemStyle: .done, action: doneAction)],
                         animated: true)
        toolBar.tintColor = .systemYellow
        self.inputAccessoryView = toolBar
    }
    
    func datePicker<T>(target: T,
                       doneAction: Selector,
                       cancelAction: Selector,
                       datePickerMode: UIDatePicker.Mode = .date) {
        
        let screenWidth = UIScreen.main.bounds.width
        let screenHeight = UIScreen.main.bounds.height
        
        let datePicker = UIDatePicker(frame: CGRect(x: 0,
                                                    y: 0,
                                                    width: screenWidth,
                                                    height: 0.4 * screenHeight))
        datePicker.datePickerMode = datePickerMode
        datePicker.tintColor = .systemYellow
        self.inputView = datePicker
        
        addToolbar(cancelAction: cancelAction, doneAction: doneAction, screenWidth: screenWidth)
    }
    
    func pickerView<T>(target: T,
                       doneAction: Selector,
                       cancelAction: Selector) {
        
        let screenWidth = UIScreen.main.bounds.width
        let screenHeight = UIScreen.main.bounds.height
        
        let pickerView = UIPickerView(frame: CGRect(x: 0,
                                                    y: 0,
                                                    width: screenWidth,
                                                    height: 0.4 * screenHeight))
        pickerView.tintColor = .systemYellow
        pickerView.delegate = target as? UIPickerViewDelegate
        self.inputView = pickerView
        
        addToolbar(cancelAction: cancelAction, doneAction: doneAction, screenWidth: screenWidth)
    }
}
