//
//  ToggleBar.swift
//  Timenizer
//
//  Created by Sergio Escalante Ordonez on 23/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

enum ToggleBarStyle {
    case Error
    case Alert
    case Success
    case Info
}

class ToggleBar: UIView {
    
    // MARK: - IBOutlets
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    
    // MARK: - Init methods
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.loadNib(by: Constants.Components.ToggleBar, contentView: contentView)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.loadNib(by: Constants.Components.ToggleBar, contentView: contentView)
    }
    
    // MARK: - IBActions
    
    @IBAction func pressCloseButton(_ sender: UIButton) {
        alertView.isHidden = true
    }
    
    // MARK: - Custom methods
    
    func showAlert(message: String, style: ToggleBarStyle) {
        
        messageLabel.text = message
        
        switch style {
            case .Error:
                alertView.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            case .Alert:
                alertView.backgroundColor = #colorLiteral(red: 0.9935260354, green: 1, blue: 0.3244675781, alpha: 1)
            case .Success:
                alertView.backgroundColor = #colorLiteral(red: 0.605601617, green: 1, blue: 0.4766704675, alpha: 1)
            case .Info:
                alertView.backgroundColor = #colorLiteral(red: 0.3854542174, green: 1, blue: 0.9990857364, alpha: 1)
        }
        
        alertView.isHidden = false
    }
}
