//
//  Constants.swift
//  Timenizer
//
//  Created by Sergio Escalante Ordonez on 23/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Storyboards {
        static let Home = "Home"
        static let Login = "Login"
        static let AddReminder = "AddReminder"
    }
    
    struct ViewControllers {
        static let AddReminderViewController = "AddReminderViewController"
    }
    
    struct Cells {
        static let ItemCell = "ItemCell"
    }
    
    struct Components {
        static let ToggleBar = "ToggleBar"
    }
}
