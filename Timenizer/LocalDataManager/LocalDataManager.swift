//
//  LocalDataManager.swift
//  Timenizer
//
//  Created by Sergio Escalante Ordonez on 23/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit.UIApplication
import CoreData

enum ReminderType: String, CaseIterable {
    case task = "task"
    case subscription = "subscription"
    case trip = "trip"
    case plan = "plan"
}

class LocalDataManager {
    
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private var reminders = [Reminder]()
    
    private func saveContext() -> Error? {
        do {
            try context.save()
            return nil
        } catch {
            print("Error saving context: \(error.localizedDescription)")
            return error
        }
    }
    
    func loadReminders(with request: NSFetchRequest<Reminder> = Reminder.fetchRequest(), and predicate: NSPredicate? = nil) -> ([Reminder], Error?) {
        
        if let predicate = predicate {
            request.predicate = predicate
        }
        
        do {
            reminders = try context.fetch(request)
            return (reminders, nil)
        } catch {
            print("Error fetching reminders: \(error)")
            return ([], error)
        }
    }
    
    func addReminder(name: String, date: Date, summary: String? = nil, priority: Bool, type: ReminderType? = .task) -> Error? {
        
        let newReminder = Reminder(context: self.context)
        
        newReminder.name = name
        newReminder.date = date
        newReminder.summary = summary
        newReminder.pritority = priority
        newReminder.type = type?.rawValue
        
        reminders.append(newReminder)
        
        return saveContext()
    }
}
