//
//  HomeModel.swift
//  Timenizer
//
//  Created by Sergio Escalante Ordonez on 23/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit.UIImage

struct HomeModel {
    
    private var items: [Item]
    private var filteredItems: [Item]
    
    func numberOfItems() -> Int {
        return filteredItems.count
    }
    
    func getItem(at index: Int) -> Item {
        return filteredItems[index]
    }
    
    mutating func filterItems(by name: String) {
        if name.isEmpty {
            filteredItems = items
        } else {
            filteredItems = items.filter({ item -> Bool in
                return item.name.contains(name)
            })
        }
    }
    
    init(_ items: [Item]) {
        self.items = items
        self.filteredItems = items
    }
}

struct Item {
    var name: String
    var date: String
    var icon: UIImage
    
    init(_ reminder: Reminder) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd E, HH:mm"
        let stringDate = dateFormatter.string(from: reminder.date!)
        
        name = reminder.name!
        date = stringDate
        let type = ReminderType(rawValue: reminder.type!)
        
        switch type {
            case .subscription:
                icon = UIImage(systemName: "tv")!
            case .task:
                icon = UIImage(systemName: "largecircle.fill.circle")!
            case .plan:
                icon = UIImage(systemName: "figure.walk")!
            case .trip:
                icon = UIImage(systemName: "airplane")!
            case .none:
                icon = UIImage(systemName: "xmark.octagon")!
        }
    }
}
