//
//  HomeViewController.swift
//  Timenizer
//
//  Created by Sergio Escalante Ordonez on 23/11/2020.
//  Copyright © 2020 Sergio Escalante Ordoñez. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {
    
    // MARK: - IBOutlets
    
    // MARK: - Variables
    var model: HomeModel? {
        didSet {
            reloadTableView()
        }
    }
    
    // MARK: - Constants
    let localDataManager = LocalDataManager()
    
    // MARK: - Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getReminders()
    }
    
    // MARK: - IBActions
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        self.navigateToAddReminder()
    }
    
    // MARK: - LocalDataManager methods
    
    func getReminders() {
        
        let (reminders, error) = self.localDataManager.loadReminders()
        
        if let _ = error {
            let alertController = UIAlertController(title: "Error fetching your reminders", message: "An unexpected error has occurred", preferredStyle: .alert)
            
            let retryAction = UIAlertAction(title: "Retry", style: .default) { [weak self] action in
                self?.getReminders()
            }
            
            alertController.addAction(retryAction)
            
            self.present(alertController, animated: true, completion: nil)
        } else {
            let items = reminders.map { (reminder) in
                return Item(reminder)
            }
            
            self.model = HomeModel(items)
        }
    }
    
    // MARK: - Actions methods
    
    // MARK: - Custom methods
    func reloadTableView() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    func navigateToAddReminder() {
        let storyboard = UIStoryboard(name: Constants.Storyboards.AddReminder, bundle: nil)
        
        if let viewController = storyboard.instantiateViewController(withIdentifier: Constants.ViewControllers.AddReminderViewController) as? AddReminderViewController {
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Delegate methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Datasource methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.numberOfItems() ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath)
        
        if let item = model?.getItem(at: indexPath.row) {
            cell.textLabel?.text = "\(item.name) - \(item.date)"
            cell.imageView?.tintColor = .systemYellow
            cell.imageView?.image = item.icon
            cell.accessoryType = .disclosureIndicator
        }
        
        return cell
    }
}

// MARK: - UISearchbar delegate methods

extension HomeViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        model?.filterItems(by: "")
        
        DispatchQueue.main.async {
            searchBar.resignFirstResponder()
        }
        
        reloadTableView()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        model?.filterItems(by: searchBar.text ?? "")

        reloadTableView()
    }
}
